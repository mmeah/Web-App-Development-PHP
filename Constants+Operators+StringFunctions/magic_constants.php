<?php
    namespace Sajal_Sarkar;

    //Constant 
    define("MY_CONSTANT", "This Is It");
    echo MY_CONSTANT . "<br/>";

    //Magic Constants 
    echo "Line: " . __LINE__ . "<br/>"; //Line Constant
    echo "File: " . __FILE__ . "<br/>"; //File Constant
    echo "Namespace: " . __NAMESPACE__ . "<br/>"; //Namespace Constant
    echo "Dir: " . __DIR__ . "<br/>"; //Dir Constant

    //Function Constant
    function my_fun(){
        echo "Some String <br/>";
        echo "Function: " . __FUNCTION__ . "<br/>";
    }
    my_fun();
    
    //Class & Method Constant
    class my_own {
        function new(){
            echo "Class: " . __CLASS__ . "<br/>";
            echo "Method: " . __METHOD__ . "<br/>";
        }
    }
    $a = new my_own; // Object
    $a -> new();

    //Trait Constant
    trait my_new {
        function hop(){
            echo "Something new for reducing class inheritance problem <br/>";
        }
    }
    class my_town {
        use my_new;
        function news(){
            echo "Thank U <br/>";
        }
    }
    $b = new my_town; // Object
    $b -> hop();
    $b -> news();